<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        return View('Admin.index');
    }
    //
    public function login(Request $request){
        $errors = new MessageBag();
        $accountFromDb = User::where('email',$request->email)->orWhere('phone',$request->phone);
        if($accountFromDb == null)
        {
            $errors->add('Login','Wrong Email/Phone or Password');
            return View('Default.login')->withErrors($errors);
        }
        if(Hash::check($request->password, $accountFromDb->password)){
        }
    }
}
