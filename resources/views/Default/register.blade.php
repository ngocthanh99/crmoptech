@extends('Layouts.layout')
@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Đăng kí tài khoản</h1>
                </div>
                <form class="user">
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Họ và tên">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="SĐT">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email">
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Mật khẩu">
                    </div>
                    <div class="col-sm-6">
                      <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Xác nhận mật khẩu">
                    </div>
                  </div>
                  <a href="login.html" class="btn btn-primary btn-user btn-block">
                    Đăng kí
                  </a>
                  <hr>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="forgot-password.html">Quên mật khẩu</a>
                </div>
                <div class="text-center">
                  <a class="small" href="login.html">Đã có tài khoản? Đăng nhập!</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection