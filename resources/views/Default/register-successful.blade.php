@extends('Layouts.layout')
@section('content')
<div class="container-fluid">

    <!-- 404 Error Text -->
    <div class="text-center">
      <div class="error mx-auto" data-text="200">200</div>
      <p class="lead text-gray-800 mb-5">Đăng kí thành công</p>
      <p class="text-gray-500 mb-0">Vui lòng chờ admin active tài khoản của bạn</p>
      <a href="index.html">&larr; Quay lại trang chính</a>
    </div>

  </div>
@endsection