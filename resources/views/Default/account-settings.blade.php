@extends('Layouts.layout')
@section('content')
<div id="wrapper">
@include('Layouts.admin-side-nav')
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    @include('Layouts.nav')
    <!-- .wrapper -->
    <div class="wrapper">
      <!-- .page -->
      <div class="page">
        <!-- .page-cover -->
        <header class="page-cover">
          <div class="text-center">
            <a class="user-avatar user-avatar-xl">
              <img width="150px"
                src="https://i.guim.co.uk/img/media/fe1e34da640c5c56ed16f76ce6f994fa9343d09d/0_174_3408_2046/master/3408.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=0d3f33fb6aa6e0154b7713a00454c83d"
                alt="User Avatar">
            </a>
            <h2 class="h4 mt-3 mb-0"> Thanh </h2>
            <div class="my-1">
              <i class="fa fa-star text-yellow"></i>
              <i class="fa fa-star text-yellow"></i>
              <i class="fa fa-star text-yellow"></i>
              <i class="fa fa-star text-yellow"></i>
              <i class="far fa-star text-yellow"></i>
            </div>
            <p class="text-muted"> Telegram: ....... </p>
            <p> Zalo:............. Bitbucket: .............</p>
          </div>
          <!-- .cover-controls -->
          <div class="cover-controls cover-controls-bottom">
            <a href="#" class="btn btn-light" data-toggle="modal" data-target="#followersModal">2,159 Followers</a>
            <a href="#" class="btn btn-light" data-toggle="modal" data-target="#followingModal">136 Following</a>
          </div>
          <!-- /.cover-controls -->
        </header>
        <!-- /.page-title-bar -->
        <!-- .page-section -->
        <div class="page-section">
          <!-- grid row -->
          <div class="row">
            <!-- grid column -->
            <div class="col-lg-4">
              <!-- .card -->
              <div class="card card-fluid">
                <h6 class="card-header"> Your Details </h6>
                <!-- .nav -->
                <nav class="nav nav-tabs flex-column">
                  <a href="user-profile-settings.html" class="nav-link">Profile</a>
                  <a href="user-account-settings.html" class="nav-link active">Account</a>
                  <a href="user-billing-settings.html" class="nav-link">Billing</a>
                  <a href="user-notification-settings.html" class="nav-link">Notifications</a>
                </nav>
                <!-- /.nav -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /grid column -->
            <!-- grid column -->
            <div class="col-lg-8">
              <!-- .card -->
              <div class="card card-fluid">
                <h6 class="card-header"> Account </h6>
                <!-- .card-body -->
                <div class="card-body">
                  <!-- form -->
                  <form method="post">
                    <!-- form row -->
                    <div class="form-row">
                      <!-- form column -->
                      <div class="col-md-6 mb-3">
                        <label for="input01">Họ và tên</label>
                        <input type="text" class="form-control" id="input01" value="Luyện Ngọc Thanh" required="">
                       </div>
                      <!-- /form column -->
                      <!-- form column --> 
                    <!-- /form row -->
                    <!-- .form-group -->
                    <div class="form-group">
                      <label for="input03">Email</label>
                      <input type="email" class="form-control" id="input03" value="bent10@looper.com" required="">
                    </div>
                    <!-- /.form-group -->
                    <!-- .form-group -->
                    <div class="form-group ml-1">
                      <label for="input04">SĐT</label>
                      <input type="password" class="form-control" id="input04" required=""> </div>
                    <!-- /.form-group -->
                    <!-- .form-group -->
                    <!-- /.form-group -->
                    <hr>
                    <!-- .form-actions -->
                    
                    </div>
                    <div>
                      <div class="form-group">
                        <label for="input01">Ảnh đại diện</label>
                        <input type="text" class="form-control" id="input01" value="Luyện Ngọc Thanh" required="">
                       </div>
                      <!-- /form column -->
                      <!-- form column --> 
                    <!-- /form row -->
                    <!-- .form-group -->
                    <div class="form-group">
                      <label for="input04">Địa chỉ</label>
                      <input type="password" class="form-control" id="input04" required=""> </div>
                    <div class="form-group">
                      <label for="input03">Quê quán</label>
                      <input type="email" class="form-control" id="input03" value="bent10@looper.com" required="">
                    </div>
                    <!-- /.form-group -->
                    <!-- .form-group -->
                   
                    <!-- /.form-group -->
                    <!-- .form-group -->
                    <!-- /.form-group -->
                    <hr>
                    </div>
                    <div class="form-actions">
                      <!-- enable submit btn when user type their current password -->
                      <button type="submit" class="btn btn-primary" disabled>Lưu</button>
                    </div>
                    <!-- /.form-actions -->
                  </form>
                  <!-- /form -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /grid column -->
          </div>
          <!-- /grid row -->
        </div>
        <!-- /.page-section -->
      </div>
      <!-- /.page-inner -->
    </div>
    <!-- /.page -->
  </div>
  <!-- /.wrapper -->
</div>
</div>
</div>
@endsection