@extends('Layouts.layout')
@section('content')
<!-- Page Wrapper -->
<div id="wrapper">

  <!-- Sidebar -->
  @include('Layouts.admin-side-nav')
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      @include('Layouts.nav')

      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Project Dashboard</h1>
          <a href="#createProjectModal" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Thêm dự án</a>
        </div>
        <div class="row mt-4">
          <input class="col-2 form-control ml-3 mr-2" type="date" name="" id="">
          <input class="col-2 form-control" type="date" name="" id="">
          <select class="form-control col-2 ml-4" name="" id="">
            <option value="">Năm</option>
            <option value="">Quý</option>
            <option value="">Tháng</option>
          </select>
        </div>
        <div class="row mt-4">
          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-3">
            <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tổng số dự án(tháng)</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">50</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-3">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="projects/on-going">Dự án đang triển khai(tháng)</a> </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">8</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar-day fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6 mb-3">
            <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Dự án hoàn thành(tháng)</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">3</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar-day fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6 mb-3">
            <div class="card border-left-secondary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Dự án đã hủy(tháng)</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">2</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar-day fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6 mb-3">
            <a href="projects/inactive-projects">
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Dự án chờ triển khai(tháng)</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">20</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
          </div>
        </div>
        <div>
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"></h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="chart-area">
                  <canvas id="myAreaChart"></canvas>
                </div>
              </div>
            </div>
        </div>
        <div id="createProjectModal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <form id="createForm" method="POST">
                      @csrf
                      <div class="modal-header">
                          <h4 class="modal-title">Thêm dự án</h4>
                          
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col">
                            <form action="" class="form-group">
                                <div class="row">
                                    <div class="col-5 ml-2">
                                        <label for="">Tên dự án</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 ml-2">
                                        <label for="">Logo</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-5 ml-2">
                                        <label for="">Chi tiết dự án</label>
                                    </div>
                                    <div class="col-7">
                                        <textarea type="" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-5 ml-2">
                                        <label for="">Leader</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="" class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-5 ml-2">
                                        <label for="">Ngày nhận dự án</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="text" class="form-control" value="12/09/2020" disabled>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-5 ml-2">
                                        <label for="">Thời gian bắt đầu</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="date" class="form-control">
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-5 ml-2">
                                        <label for="">Thời gian kết thúc</label>
                                    </div>
                                    <div class="col-7">
                                        <input type="date" class="form-control">
                                    </div>
                                </div>
                            </form>
                          </div>
                          <div class="col-auto">
                            <SECTION>
                                <DIV id="dropzone">
                                  <FORM class="dropzone needsclick" id="demo-upload" action="/upload">
                                    <DIV class="dz-message needsclick">    
                                      Upload các tài liệu liên quan đến dự án<BR>
                                      <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                                      files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                                    </DIV>
                                  </FORM>
                                </DIV>
                              </SECTION>
                              
                          </div>
                      </div>
                          <input name="_token" type="hidden" value="{{ csrf_token() }}">
                      </div>
                      <div class="modal-footer">
                          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                          <input id="createButton" type="submit" class="btn btn-info" value="Lưu">
                      </div>
                  </form>
              </div>
          </div>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection