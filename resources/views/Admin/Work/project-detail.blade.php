@extends('Layouts.layout')
@section('content')
<div id="wrapper">
  @include('Layouts.admin-side-nav')
  <div id="content-wrapper" class="d-flex flex-column">
    <div class="content">
      @include('Layouts.nav')
      <div class="container-fluid">

        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Project 1</div>
                <div>Leader: Thanh</div>
                <div>Assigned Task: 3</div>
                <div>Done: 1/3</div>
                <div class="row no-gutters align-items-center">
                  <div class="col-auto">
                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">10%</div>
                  </div>
                  <div class="col">
                    <div class="progress progress-sm mr-2">
                      <div class="progress-bar bg-info" role="progressbar" style="width: 10%" aria-valuenow="10"
                        aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class=""></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> <br>
                <a href="#prioritizeProjectModal" data-toggle="modal" class="btn btn-warning">Ưu tiên <i
                    class="fas fa-warning"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-4 container-fluid">

          <!-- Collapsable Card Example -->
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample1" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample1">
              <h6 class="m-0 font-weight-bold text-primary">Module 1 - Thời gian: 8 tiếng </h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample1">
              <div class="card-body">
                Chi tiết: This is a collapsable card example using Bootstrap's built in collapse functionality.
                <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <a href="#collapseCardTask1" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask1">
                    <h6 class="m-0 font-weight-bold text-primary">Task 1 - Như - 1 tiếng</h6>
                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask1">
                    <div class="row">
                      <div class="col">
                        <div class="card-body">
                          Chú ý:
                          <strong>Code cẩn thận</strong> nhớ comment code!
                        </div>
                      </div>
                      <div class="col-auto mt-2 mr-3"><a href="" class="btn btn-warning">Ưu tiên <i
                            class="fas fa-warning"></i></a></div>
                    </div>
                  </div>
                </div>
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <a href="#collapseCardTask2" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask2">
                    <h6 class="m-0 font-weight-bold text-primary">Task 2 - Chương - 2 tiếng</h6>
                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask2">
                    <div class="row">
                      <div class="col">
                        <div class="card-body">
                          Chú ý:
                          <strong>Code cẩn thận</strong> nhớ comment code!
                        </div>
                      </div>
                      <div class="col-auto mt-2 mr-3"><a href="" class="btn btn-warning">Ưu tiên <i
                            class="fas fa-warning"></i></a></div>
                    </div>
                  </div>
                </div>
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <a href="#collapseCardTask3" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask3">
                    <h6 class="m-0 font-weight-bold text-primary">Task 3 - Lương - 5 tiếng</h6>
                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask3">
                    <div class="row">
                      <div class="col">
                        <div class="card-body">
                          This is a collapsable card example using Bootstrap's built in collapse functionality.
                          <strong>Click on the card header</strong> to see the card body collapse and expand!
                        </div>
                      </div>
                      <div class="col-auto mt-2 mr-3"><a href="" class="btn btn-warning">Ưu tiên <i
                            class="fas fa-warning"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a></div>
          </div>
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample2" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample2">
              <h6 class="m-0 font-weight-bold text-primary">Module 2 - Thời gian: 10 tiếng</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample2">
              <div class="card-body">
                This is a collapsable card example using Bootstrap's built in collapse functionality. <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a></div>
          </div>
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample3" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample3">
              <h6 class="m-0 font-weight-bold text-primary">Module 3 - Thời gian: 15 tiếng</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample3">
              <div class="card-body">
                This is a collapsable card example using Bootstrap's built in collapse functionality. <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a></div>
          </div>

        </div>
        <div id="prioritizeProjectModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <form id="createForm" method="POST">
                @csrf
                <div class="modal-header">
                  <h4 class="modal-title">Ưu tiên</h4>

                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col">
                      <form action="" class="form-group">
                        <div class="row">
                          <div class="col-2">
                            <label for="">Chi tiết</label>
                          </div>
                          <div class="col-9">
                            <textarea type="" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                          </div>
                            <SECTION class="mt-2">
                                <DIV id="dropzone">
                                  <FORM class="dropzone needsclick" id="demo-upload" action="/upload">
                                    <DIV class="dz-message needsclick">    
                                      Upload các tài liệu liên quan<BR>
                                      <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                                      files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                                    </DIV>
                                  </FORM>
                                </DIV>
                              </SECTION>
                      <div class="modal-footer">
                          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                          <input id="createButton" type="submit" class="btn btn-info" value="Lưu">
                      </div>
                  </form>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection