@extends('Layouts.layout')
@section('content')
<div id="wrapper">
    @include('Layouts.admin-side-nav')
    <div id="content-wrapper" class="d-flex flex-column">
        <div class="content">
            @include('Layouts.nav')
            <div class="container-fluid">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Thêm dự án</h1>
                    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-backspace fa-sm text-white-50"></i> Hủy</a>
                  </div>
                  <div class="row">
                      <div class="col">
                        <form action="" class="form-group">
                            <div class="row">
                                <div class="col ml-2">
                                    <label for="">Tên dự án</label>
                                </div>
                                <div class="col-auto">
                                    <input type="" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col ml-2">
                                    <label for="">Logo</label>
                                </div>
                                <div class="col-auto">
                                    <input type="file" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col ml-2">
                                    <label for="">Chi tiết dự án</label>
                                </div>
                                <div class="col-auto">
                                    <input type="" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col ml-2">
                                    <label for="">Leader</label>
                                </div>
                                <div class="col-auto">
                                    <input type="" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col ml-2">
                                    <label for="">Ngày nhận dự án</label>
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control" value="12/09/2020" disabled>
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col ml-2">
                                    <label for="">Thời gian bắt đầu</label>
                                </div>
                                <div class="col-auto">
                                    <input type="date" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col ml-2">
                                    <label for="">Thời gian kết thúc</label>
                                </div>
                                <div class="col-auto">
                                    <input type="date" class="form-control">
                                </div>
                            </div>
                            <div class="btn btn-primary align-content-center">Thêm dự án</div>
                        </form>
                      </div>
                      <div class="col-auto">
                        <SECTION>
                            <DIV id="dropzone">
                              <FORM class="dropzone needsclick" id="demo-upload" action="/upload">
                                <DIV class="dz-message needsclick">    
                                  Upload các tài liệu liên quan đến dự án<BR>
                                  <SPAN class="note needsclick">(This is just a demo dropzone. Selected 
                                  files are <STRONG>not</STRONG> actually uploaded.)</SPAN>
                                </DIV>
                              </FORM>
                            </DIV>
                          </SECTION>
                          
                      </div>
                  </div>
                    
            </div>
        </div>
    </div>
</div>
@endsection