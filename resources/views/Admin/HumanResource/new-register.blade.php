@extends('Layouts.layout')
@section('content')
<div id="wrapper">
    @include('Layouts.admin-side-nav')
    <div id="content-wrapper" class="d-flex flex-column">
        <div class="content">
            @include('Layouts.nav')
            <div class="container-fluid">
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Danh sách tài khoản chờ active</h1>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tfoot>
                              <tr>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Action</th>
                              </tr>
                            </tfoot>
                            <tbody>
                              <tr>
                                <td>Tiger Nixon</td>
                                <td>test@gmail.com</td>
                                <td>0123239123</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                    <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                              <tr>
                                <td>Garrett Winters</td>
                                <td>test1@gmail.com</td>
                                <td>0923239123</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                  <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                              <tr>
                                <td>Ashton Cox</td>
                                <td>test2@gmail.com</td>
                                <td>0124239122</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                  <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                              <tr>
                                <td>Cedric Kelly</td>
                                <td>test3@gmail.com</td>
                                <td>0123334441</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                  <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                              <tr>
                                <td>Airi Satou</td>
                                <td>test4@gmail.com</td>
                                <td>0444222112</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                  <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                              <tr>
                                <td>Brielle Williamson</td>
                                <td>test5@gmail.com</td>
                                <td>0123539123</td>
                                <td><a href="#" class="btn btn-success">Approve</a>
                                  <a href="#" class="btn btn-secondary">Destroy</a></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
          
                  </div>
                  <!-- /.container-fluid -->
          
                </div>
            </div>
        </div>
    </div>
</div>

@endsection