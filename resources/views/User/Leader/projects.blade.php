@extends('Layouts.layout')
@section('content')
<div id="wrapper">
  @include('Layouts.user-side-nav')
  <div id="content-wrapper" class="d-flex flex-column">
    <div class="content">
      @include('Layouts.nav')
      <div class="container-fluid">

        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Dự án MT7</div>
                <div>Tổng số module: 3</div>
                <div>Tổng số task: 9</div>
                <div class="row no-gutters align-items-center">
                  <div class="col-auto">
                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                  </div>
                  <div class="col">
                    <div>Tiến độ hoàn thành</div>
                    <div class="progress progress-sm mr-2">
                      <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50"
                        aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                <br>
                <a href="#createModuleModal" data-toggle="modal" class="btn btn-primary">Thêm module</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-4">

          <!-- Collapsable Card Example -->
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample1" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample1">
              <h6 class="m-0 font-weight-bold text-primary">Module 1 - Thời gian: 8 tiếng </h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample1">
              <div class="card-body">
                Chi tiết: This is a collapsable card example using Bootstrap's built in collapse functionality.
                <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <a href="#collapseCardTask1" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask1">
                    <h6 class="m-0 font-weight-bold text-primary">Task 1 - Như - 1 tiếng</h6>
                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask1">
                    <div class="row">
                      <div class="col">
                        <div class="card-body">
                          Chú ý:
                          <strong>Code cẩn thận</strong> nhớ comment code!
                        </div>
                      </div>
                      <div class="col-auto mt-2 mr-3"><a href="" class="btn btn-primary mr-2">Sửa</a> <a href=""
                          class="btn btn-danger mr-2">Xóa</a> <a href="" class="btn btn-warning">Ưu tiên <i
                            class="fas fa-warning"></i></a></div>
                    </div>
                  </div>
                </div>
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <div class="card-header">
                    <a class="btn btn-warning btn-icon-split" href="#prioritizeProjectModal" data-toggle="modal"><span
                        class="icon"><i class="fas fa-exclamation-triangle"></i></span><span class="text">Ưu
                        tiên</span></a>
                  </div>
                  <a href="#collapseCardTask2" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask2">
                    <h6 class="m-0 font-weight-bold text-primary">Task 2 - Chương - 2 tiếng</h6>

                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask2">
                    <div class="row card-body">
                      <div class="col-2 mt-1"><select class="form-control" name="" id="">
                          <option value="Thanh">Thanh</option>
                          <option value="Lương">Lương</option>
                          <option value="Thảo">Thảo</option>
                        </select></div>
                      <div class="col-2 mt-1">Thời gian: 1 tiếng<input class="form-group" type="range" min="1" max="8">
                      </div>
                      <div class="col-3 ml-1">Note<textarea class="form-control mt-1"
                          value="This is a collapsable card example using Bootstrap's built in collapse functionality"></textarea>
                      </div>
                      Ưu tiên <input type="checkbox" name="" id="">
                      <div class="col-auto mt-1 mr-1"><a href="" class="btn btn-primary mr-2">Lưu</a><a href=""
                          class="btn btn-secondary">Hủy</a></div>
                    </div>
                  </div>
                </div>
                <div class="card shadow mb-4">
                  <!-- Card Header - Accordion -->
                  <a href="#collapseCardTask3" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="collapseCardTask3">
                    <h6 class="m-0 font-weight-bold text-primary">Task 3 - Lương - 5 tiếng</h6>

                  </a>
                  <!-- Card Content - Collapse -->
                  <div class="collapse show" id="collapseCardTask3">
                    <div class="row">
                      <div class="col">
                        <div class="card-body">
                          This is a collapsable card example using Bootstrap's built in collapse functionality.
                          <strong>Click on the card header</strong> to see the card body collapse and expand!
                        </div>
                      </div>
                      <div class="col-auto mt-2 mr-3"><a href="" class="btn btn-primary mr-2">Sửa</a> <a href=""
                          class="btn btn-danger mr-2">Xóa</a> <a href="" class="btn btn-warning">Ưu tiên <i
                            class="fas fa-warning"></i></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a><a
                href="" class="btn btn-primary ml-2">Thêm task <i class="fas fa-plus"></i></a></div>
          </div>
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample2" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample2">
              <h6 class="m-0 font-weight-bold text-primary">Module 2 - Thời gian: 10 tiếng</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample2">
              <div class="card-body">
                This is a collapsable card example using Bootstrap's built in collapse functionality. <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a><a
                href="" class="btn btn-primary ml-2">Thêm task <i class="fas fa-plus"></i></a></div>
          </div>
          <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample3" class="d-block card-header py-3" data-toggle="collapse" role="button"
              aria-expanded="true" aria-controls="collapseCardExample3">
              <h6 class="m-0 font-weight-bold text-primary">Module 3 - Thời gian: 15 tiếng</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample3">
              <div class="card-body">
                This is a collapsable card example using Bootstrap's built in collapse functionality. <strong>Click on
                  the card header</strong> to see the card body collapse and expand!
              </div>
            </div>
            <div class="card-footer"><a href="" class="btn btn-warning">Ưu tiên <i class="fas fa-warning"></i></a><a
                href="" class="btn btn-primary ml-2">Thêm task <i class="fas fa-plus"></i></a></div>
          </div>

        </div>
        <div id="createModuleModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <form id="createForm" method="POST">
                @csrf
                <div class="modal-header">
                  <h4 class="modal-title">Thêm Module</h4>

                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  CRUD Module<input class="form-control" type="checkbox" name="" id="">
                  <div class="form-group">
                    <div id="usernameWarning" class="alert alert-warning alert-dismissible fade show" hidden
                      role="alert">
                      <strong></strong>
                    </div>
                    <label>Tên module</label>
                    <input id="username" type="text" name="username" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Mô tả</label>
                    <textarea id="detail" type="text" class="form-control" name="fullname"></textarea>
                  </div>
                  <div class="form-group">
                    <label>Độ ưu tiên</label>
                    <input type="text" class="form-control" name="fullname">
                  </div>
                  <div class="form-group row">
                    <div class="col-5 mt-2"><label>Thời gian kéo dài</label></div>
                    <div class="col-5 mt-1"><input id="email" type="text" class="form-control" name="email" />
                    </div>
                    <div class="col-2 mt-2">Giờ</div>
                  </div>
                  <div class="form-group">
                    <label>Giao việc</label>
                    <select class="form-control" name="" id="">
                      <option value="">Thanh</option>
                      <option value="">Lương</option>
                      <option value="">Thảo</option>
                    </select>
                  </div>
                  <input name="_token" type="hidden" value="{{ csrf_token() }}">
                </div>
                <div class="modal-footer">
                  <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                  <input id="createButton" type="submit" class="btn btn-info" value="Lưu">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div id="prioritizeProjectModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <form id="createForm" method="POST">
                @csrf
                <div class="modal-header">
                  <h4 class="modal-title">Ưu tiên</h4>

                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <form action="" class="form-group">
                        <div class="row">
                          <div class="col-2">
                            <label for="">Chi tiết</label>
                          </div>
                          <div class="col-9">
                            <textarea type="" class="form-control" value="">Sửa cái này sửa cái này,....</textarea>
                          </div>
                        </div>
                      </form>
                    <SECTION class="mt-2">
                      <img width="200px" src="https://i.ytimg.com/vi/1Ne1hqOXKKI/maxresdefault.jpg" alt="">
                    </SECTION>
                    <div class="modal-footer">
                      <input type="button" class="btn btn-default" data-dismiss="modal" value="Hủy">
                      <input id="createButton" type="submit" class="btn btn-info" value="Bắt đầu">
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $(document).on('keyup', '#detail', function () {
      detail = $(this).val();
      console.log(detail);
    })
  })
</script>
@endsection