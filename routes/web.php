<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('', function () {
    return view('Default.login');
});
Route::get('project', function () {
    return view('Admin.work.project-detail');
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('projects', function () {
        return view('Admin.Work.projects-dashboard');
    });
    Route::get('projects/on-going', function () {
        return view('Admin.Work.projects');
    });
    Route::get('projects/project-detail',function(){
        return view('Admin.Work.project-detail');
    });
    Route::get('', 'UserController@index');
    Route::get('users', function () {
        return view('Admin.HumanResource.index');
    });
    Route::get('users/new-register', function () {
        return view('Admin.HumanResource.new-register');
    });
    Route::get('projects/create-project', function () {
        return view('Admin.Work.create-project');
    });
    Route::get('projects/inactive-projects', function () {
        return view('Admin.Work.inactive-projects');
    });
});
Route::group(['prefix' => 'user'], function () {
    Route::get('', function () {
        return view('User.index');
    });
    Route::get('leader', function(){
        return view('User.Leader.projects');
    });
});
Route::get('users/account-settings', function () {
    return view('Default.account-settings');
});
Route::get('register', function () {
    return view('Default.register');
});
Route::get('register-successful', function () {
    return view('Default.register-successful');
});
